/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        Scanner scanner = new Scanner(System.in);

        try {
            // Memasukkan nama pemesan
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();

            // Validasi panjang nama pemesan
            if (namaPemesan.length() >= 10) {
                throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter!");
            }

            // Memilih jenis tiket
            TiketKonser.MenuTiket();

            System.out.print("Masukkan pilihan tiket (1-5): ");
            int pilihan = scanner.nextInt();
            scanner.nextLine();

            // Membuat objek PemesananTiket berdasarkan pilihan tiket
            PemesananTiket pemesananTiket = TiketKonser.pilihTiket(pilihan);

            // Memproses pemesanan tiket
            pemesananTiket.pesanTiket(namaPemesan);

            // Menampilkan detail pesanan
            pemesananTiket.DetailPesanan();
        } catch (NumberFormatException e) {
            System.out.println("Terjadi kesalahan: Pilihan tiket harus berupa angka!");
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}