package Contest;

interface HargaTiket {
    //Do your magic here...
    public static final double CAT8 = 500000;
    public static final double CAT1 = 1000000;
    public static final double FESTIVAL = 1500000;
    public static final double VIP = 2000000;
    public static final double VVIP = 3000000;

}