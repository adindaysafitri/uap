package Contest;

class VIP extends TiketKonser {

    public VIP() {
        namaTiket = "VIP";
        hargaTiket = HargaTiket.VIP;
    }

    @Override
    public void pesanTiket(String namaPemesan) {
        this.namaPemesan = namaPemesan;
    }
}