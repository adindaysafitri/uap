package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    public static void MenuTiket() {
        System.out.println("Jenis Tiket:");
        System.out.println("1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. VVIP (UNLIMITED EXPERIENCE)");
    }

    public static PemesananTiket pilihTiket(int pilihan) {
        switch (pilihan) {
            case 1:
                return new CAT8();
            case 2:
                return new CAT1();
            case 3:
                return new FESTIVAL();
            case 4:
                return new VIP();
            case 5:
                return new VVIP();
            default:
                throw new IllegalArgumentException("Pilihan tiket tidak valid!");
        }
    }
}