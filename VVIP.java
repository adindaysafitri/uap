package Contest;

class VVIP extends TiketKonser {
    // Do your magic here...
    public VVIP() {
        namaTiket = "VVIP (UNLIMITED EXPERIENCE)";
        hargaTiket = HargaTiket.VVIP;
    }

    @Override
    public void pesanTiket(String namaPemesan) {
        this.namaPemesan = namaPemesan;
    }
}